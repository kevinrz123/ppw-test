# Generated by Django 2.1.1 on 2018-10-10 14:58

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='time',
            field=models.CharField(default=datetime.datetime(2018, 10, 10, 21, 58, 42, 350258), max_length=200),
        ),
    ]
