from re import search

from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
from selenium.webdriver.chrome.options import Options
import time



# Create your tests here.


class ProfUnitTest(TestCase):

    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_addstatus_exist(self):
        response = Client().get('/leavestatus/addstatus/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/leavestatus/')
        self.assertEqual(found.func, views.index)

    def test_using_addstatus_func(self):
        found = resolve('/leavestatus/addstatus/')
        self.assertEqual(found.func, views.addstatus)

    def test_landing_page(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Kevin</title>', html_response)
        self.assertIn('<h1 name="judul">Hello, Apa kabar?</h1>', html_response)

    def test_landing_html(self):
        response = Client().get('/leavestatus/')
        self.assertTemplateUsed(response, 'index.html')

    def test_addstatus_html(self):
        response = Client().get('/leavestatus/addstatus/')
        self.assertTemplateUsed(response, 'addstatus.html')

    def test_model_can_create_new_activity(self):
        new_activity = Status.objects.create(status="Ini Status Test")
        all_activities = Status.objects.all().count()
        self.assertEqual(all_activities, 1)

    def test_form_exist(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form id="form" method="POST" action="/leavestatus/addstatus/">', html_response)

    def test_submit_button_exist(self):
        request = HttpRequest()
        response = views.index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<button type="submit" id="submit">', html_response)

    def test_auto_redirect(self):
        request = HttpRequest()
        response = views.addstatus(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<meta http-equiv="refresh"', html_response)

    def test_url_profile_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_profile_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'profil.html')

    def test_using_profil_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.profil)




class fun_test(TestCase):
    def setUp(self):
        # self.browser = webdriver.Chrome()
        # chrome_options = Options()
        # self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)


        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(fun_test, self).setUp()

    def tearDown(self):
        # self.browser.quit()
        self.selenium.quit()
        super(fun_test, self).tearDown()

    def test_input_status_selenium(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')
		
        form = selenium.find_element_by_name('status')
        submit = selenium.find_element_by_id('submit')

        form.send_keys('coba coba')
        submit.send_keys(Keys.RETURN)
        
		
    def test_status_exist(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')
        self.assertIn("<h4>coba coba</h4>", selenium.page_source)


#     Challenge ###########################################
# Test Layout
    def test_width_input_field(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')

        size = selenium.find_element_by_name('status').size
        # size = form.size

        self.assertEqual(173, size['width'])

    def test_width_submit_button(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')

        size = selenium.find_element_by_id('submit').size
        self.assertEqual(100, size['width'])


#     Test CSS property
    def test_body_color_is_white(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')

        body_color = selenium.find_element_by_name('body').value_of_css_property('background-color')
        self.assertEqual('rgba(255, 255, 255, 1)', body_color)

    def test_text_color_is_black(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/leavestatus')

        text_color = selenium.find_element_by_name('judul').value_of_css_property('color')
        self.assertEqual('rgba(0, 0, 0, 1)', text_color)
#     End of Challenge #######################################


if __name__ == '__main__':
    unittest.main(warnings='ignore')