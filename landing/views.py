from django.http import HttpResponse
from django.shortcuts import render
from .forms import statusForm, registerForm
from .models import Status, Subscriber
from django.http import JsonResponse
from json import *
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

def index(request):
    form = statusForm()

    response = {'form': form, 'all': Status.objects.all().reverse()}
    return render(request, 'index.html', response)





def addstatus(request):
    response = {}
    if request.method == 'POST':
        form = statusForm(request.POST or None)
        if form.is_valid():
            response['status'] = request.POST['status']
            status = Status(status=response['status'])

            status.save()
            return render(request, 'addstatus.html', response)

    else:
        return render(request, 'addstatus.html')


def profil(request):
    return render(request, 'profil.html')


def subscribe(request):
    form = registerForm()
    context = {
        'form': form
    }
    return render(request, 'subsForm.html', context)

def addSubscriber(request):
    response = {}
    if request.method == 'POST':
        form = registerForm(request.POST or None)
        if form.is_valid():
            print('MASUk isValid')
            print(request.POST)
            response['name'] = request.POST['name']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']

            subs = Subscriber(name=response['name'], email=response['email'], password=response['password'])

            subs.save()
            return render(request, 'addstatus.html', response)
        else:
            print(request.POST)
            print('SEBUAH DATA : --')

            return render(request, 'addstatus.html')

    else:
        print('Masuk bukan POST')
        return render(request, 'addstatus.html')

@csrf_exempt
def validate_email(request):
    email = request.POST.get('email', None)
    data = {
        'is_taken' : Subscriber.objects.filter(email=email).exists()
    }
    return JsonResponse(data)


