from .models import Status
from django import forms


class statusForm(forms.Form):
    status = forms.CharField(widget=forms.TextInput({'class': 'form-control'}), max_length=300)


class registerForm(forms.Form):
    atributes = {'class': 'form-control'}

    name = forms.CharField(label='Name', widget=forms.TextInput(attrs=atributes), max_length=300, required=True)
    email = forms.EmailField(label='Email', max_length=254, required=True, widget=forms.EmailInput(atributes))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs=atributes), min_length=6,
                               required=True, max_length=300)
